def balancedSums(arr):
    l = len(arr)
    left = 0
    right = 0
    for i in range(0,l):
        right += arr[i]
    for i in range(0,l):
        if(i - 1 >= 0):
            left+=arr[i-1]
        right-=arr[i]
        if(left == right):
            return "YES"
    return "NO"

print(balancedSums([1,2,3,3]))
print(balancedSums([1,2,3,4]))
print(balancedSums([2,0,0,0]))
print(balancedSums([1,2,3,3]))