package main

import "fmt"

func insertionSort(arr []int32) int32 {
	var index int
			var count int
			var tmp int32
	for i:=1; i < len(arr); i++{        
					if(arr[i] < arr[i - 1]){
							index = int(busquedaBinaria(arr[0:i], arr[i]))
							
							tmp = arr[i]
							for j := i; j > index; j-- {
									arr[j] = arr[j-1]
							}
							arr[index] = tmp
							count = i - index + count

					}
			}
	return int32(count)
}

func busquedaBinaria(lista []int32, elemento int32) int32{
	var index int32;
	var mitad int32;
	index = 0;
	for true{
			if(len(lista) == 1){
					if(lista[0] > elemento){
							return index;
					}else{
							return index + 1;
					}
			}else{
					mitad = int32(len(lista) / 2)
					if(lista[mitad] > elemento){
							lista = lista[0:mitad]
					}else{
							lista = lista[mitad:]
							index = index + mitad
					}
			}
			}
			
			return -1
}

func main(){
	primes := []int32{12, 15, 1, 5, 6, 14, 11}
	fmt.Printf("%v\n",insertionSort(primes))
}