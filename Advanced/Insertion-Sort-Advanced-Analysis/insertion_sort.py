def insertionSort(arr):
    return mergeSort(arr)[1]

def mergeSort(arr):
    l = len(arr)
    if(l == 1):
        return arr, 0
    middle = l//2
    left, s = mergeSort(arr[0:middle])
    right, s2 = mergeSort(arr[middle:])
    return arr, merge(arr,left,right) + s + s2

def merge(arr, left, right):
    i, j, k, count = 0,0,0, 0

    l = len(left)
    r = len(right)
    a = len(arr)
    while(i < a):
        if(k >= l):
            arr[i] = right[j]
            j+=1
        else:
            if(j >= r):
                arr[i] = left[k]
                k+=1
            else:
                if(left[k] <= right[j]):
                    arr[i] = left[k]
                    k+=1
                else:
                    arr[i] = right[j]
                    j+=1
                    count+=l - k
        i+=1
    return count


# OTra solución pero menos eficiente.
# def insertionSort(arr):
# 		count = 0
# 		for i in range(1, len(arr)):
# 				if(arr[i] < arr[i - 1]):
# 					index = busquedaBinaria(arr[0:i], arr[i])
# 					tmp = arr[i]
# 					del arr[i]
# 					arr.insert(index, tmp)
# 					count = i - index + count

# 		return count

# def busquedaBinaria(lista, elemento):
#     index = 0
#     while(True):
#         mitad = int(len(lista) / 2)
#         if(len(lista) == 1):
#             if(index == 0 and lista[0] > elemento):
#               return 0
#             if(lista[0] > elemento):
#               return index - 1
#             else:
#               return index + 1
#         else:
#             if(lista[mitad] > elemento):
#               	lista = lista[0:mitad]
#             else:
#                 lista = lista[mitad:]
#                 index += mitad
#     return -1



print(insertionSort([1,1,1,2,2]))
print(insertionSort([2,1,3,1,2]))
print(insertionSort([12 ,15 ,1 ,5, 6, 14, 11]))
insertionSort([3 ,5 ,7 ,11 ,9])

			