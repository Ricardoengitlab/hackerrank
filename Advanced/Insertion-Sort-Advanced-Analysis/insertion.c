#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();
char** split_string(char*);


int busquedaBinaria(int* lista, int fin, int elemento){
    int index, mitad, inicio;
		inicio = 0;
    index = 0;
		mitad = (fin + inicio) / 2;
		int tmp;
    while (inicio <= fin) {
			// printf("%d %d %d %d %d\n", inicio, fin, mitad, elemento, tmp);
      if(lista[mitad] < elemento){
				inicio = mitad + 1;
			}else{
				if(lista[mitad] == elemento){
					return mitad + 1;
				} else{
					fin = mitad - 1;
				}
			}   
			mitad = (fin + inicio) / 2;
		}

		if(inicio > fin) {
			return inicio;
		}

		return -1;
}

// Complete the insertionSort function below.
int insertionSort(int arr_count, int* arr) {
    int index, count, tmp;

    for (int i = 1; i < arr_count; i++) {
        if(arr[i] < arr[i -1]){
				// printf("%d\n", arr[i]);
        index = busquedaBinaria(arr,i, arr[i]);
				// printf("Index: %d\n", index);
        tmp = arr[i];
				for(int j = i; j > index; j--){
						arr[j] = arr[j-1];
				}
        arr[index] = tmp;
        count = i - index + count;
        }
    }
    return count;
}


int main() {
	int arr[5] = {2,1,3,1,2};
	int result = insertionSort(5, arr);
	printf("%d\n", result);
}